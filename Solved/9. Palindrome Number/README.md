# 9. Palindrome Number

## Description

Determine whether an integer is a palindrome. Do this without extra space.

## Example

```
2147483648 -> false
-2147483648 -> false
1234321 -> true
1 -> true
```

## Solution

偷懒