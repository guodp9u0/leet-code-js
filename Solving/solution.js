// 662. Maximum Width of Binary Tree
/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
var widthOfBinaryTree = function(root) {

};


// TEST:

function TreeNode(val) {
    this.val = val;
    this.left = this.right = null;
}